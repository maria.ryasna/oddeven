﻿using System;
using System.Collections.Generic;

namespace OddEven
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int from = 1, to = 100;
            try
            {
                List<string> numbers = OddEvenService.GetValuesFromRange(from, to);
                foreach (string number in numbers)
                {
                    Console.Write($"{number} ");
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine($"[ERROR]: {ex.Message}");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"[ERROR]: {ex.Message}");
            }
            
            Console.ReadKey();
        }
    }
}
